# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

alias ls='ls -l --color=auto'
alias lsa='ls -la --color=auto'
alias lsblk='lsblk -o NAME,RM,SIZE,RO,TYPE,FSTYPE,SIZE,MOUNTPOINT'

function f_cd () {
    cd "$@" && ls
}
alias cd='f_cd'

function f_mkdircd () {
    mkdir "$@" && cd "$@"
}
alias mkdircd='f_mkdircd'

man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}