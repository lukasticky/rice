# Fedora Rice

Run this command to fire up your stove:

```bash
git clone https://gitlab.com/lukasticky/rice.git /tmp/rice && cd /tmp/rice && sudo chmod +x rice.sh && sudo ./rice.sh
```

You may have to run this command afterwards:

```bash
dconf load / < /tmp/rice/dconf
```