#!/bin/bash

# vibe check
if [ "$EUID" -ne 0 ]; then
    zenity --error --text="Please run as root"
    exit
fi

# get user "configuration"
input=$(zenity --forms \
    --title="Preparing rice" \
    --text="Configure your system" \
    --add-entry="Hostname" \
    --add-combo="Packages" \
    --combo-values="Developer|Basic")
hostname=$(echo $input | cut -f 1 -d "|" )
packages=$(echo $input | cut -f 2 -d "|" )

# input validation
if [ -z "$hostname" ]; then
    zenity --error --text="Please set a hostname"
    exit
fi
if [ -z "$packages" ]; then
    zenity --error --text="Please set a package bundle"
    exit
fi

# do stuff
(
    echo "# Setting hostname"
    echo $hostname > /etc/hostname

    echo "# Configuring Bash"
    cp /tmp/rice/.bashrc /home/$SUDO_USER/.bashrc

    echo "# Setting wallpaper"
    cp -r Wallpapers /home/$SUDO_USER/Pictures/
    chown -R $SUDO_USER:wheel /home/$SUDO_USER/Pictures/Wallpapers

    echo "# Importing dconf"
    sed -i "s/%USERNAME%/$SUDO_USER/g" /tmp/rice/dconf
    dconf load / < /tmp/rice/dconf

    echo "# Changing home structure"
    rm -d /home/$SUDO_USER/{Desktop,Templates}
    sed -i 's/XDG_TEMPLATES_DIR="$HOME\/Templates"/XDG_TEMPLATES_DIR="$HOME\/.Templates"/g' /home/$SUDO_USER/.config/user-dirs.dirs
    mkdir /home/$SUDO_USER/.Templates/
    touch /home/$SUDO_USER/.Templates/{Markdown.md,Text.txt}
    chown -R $SUDO_USER:wheel /home/$SUDO_USER/.Templates

    echo "# Installing software repositories"
    dnf install -y http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    echo "# Removing unwanted software"
    dnf remove -y firefox libreoffice-* rhythmbox

    echo "# Installing new software\nThis will take a while"
    dnf install -y epiphany fuse-exfat gnome-tweaks
    flatpak install -y --noninteractive com.bitwarden.desktop com.discordapp.Discord com.frac_tion.teleport com.github.bilelmoussaoui.Authenticator com.github.johnfactotum.Foliate com.valvesoftware.Steam de.haeckerfelix.Fragments de.wolfvollprecht.UberWriter org.gnome.Fractal org.gnome.Games org.gnome.Geary org.gnome.Music org.gnome.Notes org.gnome.PasswordSafe org.gtk.Gtk3theme.Adwaitaßdark org.libreoffice.LibreOffice org.telegram.desktop
    if [ $packages = "Developer" ]; then
        flatpak install -y --noninteractive com.visualstudio.code.oss org.gnome.Builder org.gnome.Glade
    fi

    echo "# Installing updates"
    dnf update -y
    flatpak update -y --noninteractive

    echo "# Your rice is done!"
) |
zenity --progress \
    --title="Cooking rice" \
    --text="Cooking rice" \
    --pulsate
